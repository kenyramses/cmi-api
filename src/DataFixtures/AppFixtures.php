<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface $userPasswordHasher
     */
    private $userPasswordHasher ;

    /**
     * AppFixture Constructor
     */
    public function __construct( UserPasswordHasherInterface $userPasswordHasher) {
        $this->userPasswordHasher = $userPasswordHasher;
    }
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $users = [];
        for ($i = 1;  $i <= 10; $i++) {
            // Create users fixtures
            $user = User::createUser(
                sprintf("ramses%d@email.com", $i),
                sprintf("keny%d", $i),
                sprintf("email%d@email.com", $i),
            );
            $user->setPassword($this->userPasswordHasher->hashPassword($user, sprintf("password%d", $i)));

            $manager->persist($user);
            $users[] = $user;
        }

        foreach ($users as $user) {
            // Create Articles fixtures
            for ($j = 1; $j <= 5; $j++) {
                $article = Article::createArticle("Lorem ipsum", $user);
                $manager->persist($article);

                // Create Comments fixtures
                for ($k = 1; $k <= 15; $k++) {
                    $comment = Comment::createComment(sprintf("message lorem ipsum %d", $k), $users[array_rand($users)], $article);
                    $manager->persist($comment);
                }
            }
        }

        $manager->flush();
    }
}
