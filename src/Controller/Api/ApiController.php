<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiController
 */
class ApiController extends AbstractController
{
    public function responseContentNotFound(): JsonResponse
    {
        return $this->json([
            'status' => Response::HTTP_NOT_FOUND,
            'message' => 'Resource not found'
        ], Response::HTTP_NOT_FOUND);
    }

    public function responseContentSuccess(): JsonResponse
    {
        return $this->json([
            'status' =>Response::HTTP_OK,
            'message' => 'Success!'
        ], Response::HTTP_NO_CONTENT);
    }

    public function responseBadRequest($errors): JsonResponse
    {
        return $this->json($errors, Response::HTTP_BAD_REQUEST);
    }

    public function response($data, int $status, ?array $group): JsonResponse
    {
        return $this->json($data, $status, [], $group);
    }

    protected function transformJsonContent(Request $request): Request
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }
}