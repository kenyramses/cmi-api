<?php

namespace App\Controller\Api\v1\Security;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GitlabController extends AbstractController
{
    /**
     * Link to this controller to start the "connect" process
     *
     * @Route("/connect/gitlab", name="connect_gitlab_start")
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        // will redirect to Gitlab!
        return $clientRegistry
            ->getClient('gitlab_main') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect([
                'public_profile', 'email' // the scopes you want to access
            ]);
    }

    /**
     * After going to Gitlab, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml
     *
     * @Route("api/connect/gitlab/check", name="connect_gitlab_check")
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
        if (!$this->getUser())
            return new JsonResponse(['status' => false, 'message' => 'User not found']);
        // return somewhere
    }

}