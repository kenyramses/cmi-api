<?php

namespace App\Controller\Api\v1;

use App\Controller\Api\ApiController;
use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CommentController
 */
class CommentController extends ApiController
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var CommentRepository $commentRepository */
    private $commentRepository;

    /** @var ArticleRepository $articleRepository */
    private $articleRepository;

    /** @var SerializerInterface $serializer */
    private $serializer;

    /** @var ValidatorInterface $validator */
    private $validator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param CommentRepository $commentRepository
     * @param ArticleRepository $articleRepository
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        CommentRepository $commentRepository,
        ArticleRepository $articleRepository,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->commentRepository = $commentRepository;
        $this->articleRepository = $articleRepository;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @Route("/v1/member/comments", name="api_v1_comments", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getComments(): JsonResponse
    {
        return $this->response($this->commentRepository->findAll(), Response::HTTP_OK, ['groups' => 'article:read']);
    }

    /**
     * @Route("/v1/member/comment", name="api_v1_add_comment_article", methods={"POST"})
     */
    public function store(Request $request): JsonResponse
    {
        $request = $this->transformJsonContent($request);

        if (!$request->get('id') || !$request->get('message')) {
            return $this->responseBadRequest('Bad parameters');
        }

        $articleId = $request->get('id');
        $message = $request->get('message');

        /** @var Article $article */
        $article = $this->articleRepository->findOneBy(['id' => (int) $articleId]);

        if (!$article)
            return $this->responseContentNotFound();

        /** @var User $user */
        $user = $this->getUser(); // Only registered users can add comments

        $comment = new Comment();
        $comment->setMessage($message);
        $comment->setArticle($article);
        $comment->setAuthor($user);

        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        return $this->response($comment, Response::HTTP_CREATED, ['groups' => 'article:read']);
    }

    /**
     *  @Route("/v1/member/comment", name="api_v1_comment", methods={"GET"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function read(Request $request): JsonResponse
    {
    }

    /**
     * @Route("/v1/member/comment", name="api_v1_comment_update", methods={"PUT"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
    }

    /**
     * @Route("/v1/secure/comment", name="api_v1_comment_approuve", methods={"PUT"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function approve(Request $request): JsonResponse
    {
        $request = $this->transformJsonContent($request);

        if (!$request->get('id')) {
            return $this->responseBadRequest('Bad parameters');
        }

        /** @var Comment $article */
        $comment = $this->commentRepository->findOneBy(['id' => (int) $request->get('id')]);

        if (!$comment)
            return $this->responseContentNotFound();

        // Set comment true to approve
        $comment->setApproved(true);

        $this->entityManager->flush();

        return  $this->responseContentSuccess();
    }

    /**
     * @Route("/v1/secure/comment", name="api_v1_comment_delete", methods={"DELETE"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $request = $this->transformJsonContent($request);

        if (!$request->get('id')) {
            return $this->responseBadRequest('Bad parameters');
        }

        /** @var Comment $article */
        $comment = $this->commentRepository->findOneBy(['id' => (int) $request->get('id')]);

        if (!$comment)
            return $this->responseContentNotFound();

        $this->entityManager->remove($comment);
        $this->entityManager->flush();

        return  $this->responseContentSuccess();
    }

    /**
     * @Route("/v1/member/comment", name="api_v1_delete_own_comment", methods={"DELETE"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteComment(Request $request): JsonResponse
    {
        // @todo
    }
}
