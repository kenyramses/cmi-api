<?php

namespace App\Controller\Api\v1;

use App\Controller\Api\ApiController;
use App\Entity\Article;
use App\Entity\User;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ArticleController
 */
class ArticleController extends ApiController
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var ArticleRepository $articleRepository */
    private $articleRepository;

    /** @var SerializerInterface $serializer */
    private $serializer;

    /** @var ValidatorInterface $validator */
    private $validator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ArticleRepository $articleRepository
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ArticleRepository $articleRepository,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->articleRepository = $articleRepository;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @Route("/v1/member/articles", name="api_v1_articles", methods={"GET"})
     * @return JsonResponse
     */
    public function getArticles(): JsonResponse
    {
        return $this->response($this->articleRepository->findAll(), Response::HTTP_OK, ['groups' => 'article:read']);
    }

    /**
     * @Route("/v1/secure/article", name="api_v1_post_article", methods={"POST"})
     */
    public function store(Request $request)
    {
        $content = $request->getContent();
        try {
            /** @var Article $article */
            $article = $this->serializer->deserialize($content, Article::class, 'json');

            /** @var User $user */
            $user = $this->getUser();
            $article->setAuthor($user);

            $errors = $this->validator->validate($article);

            if (count($errors) > 0)
                return $this->responseBadRequest($errors);

            $this->entityManager->persist($article);
            $this->entityManager->flush();

            return $this->response($article, Response::HTTP_CREATED, ['groups' => 'article:read']);
        } catch (NotEncodableValueException $e) {
            return  $this->json([
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/v1/member/article", name="api_v1_get_article", methods={"GET"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function read(Request $request): JsonResponse
    {
        $request = $this->transformJsonContent($request);

        if (!$request->get('id')) {
            return $this->responseBadRequest('Bad parameters');
        }

        $articleId = $request->get('id');

        /** @var Article $article */
        $article = $this->articleRepository->findOneBy(['id' => (int) $articleId]);

        if (!$article)
            return $this->responseContentNotFound();

        return $this->response($article, Response::HTTP_OK, ['groups' => 'article:read']);
    }

    /**
     * @Route("/v1/secure/article", name="api_v1_article_update", methods={"PUT"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        try {
            /** @var Article $article */
            $article = $this->serializer->deserialize($request->getContent(), Article::class, 'json');

            $errors = $this->validator->validate($article);

            if (count($errors) > 0)
                return $this->responseBadRequest($errors);

            $request = $this->transformJsonContent($request);

            if (!$request->get('id') || !$request->get('content')) {
                return $this->responseBadRequest('Bad parameters');
            }

            $articleId = $request->get('id');
            $content = $request->get('content');

            /** @var Article $article */
            $article = $this->articleRepository->findOneBy(['id' => (int) $articleId]);

            if (!$article)
                return $this->responseContentNotFound();

            $article->setContent($content);

            $this->entityManager->flush();

            return $this->response($article, Response::HTTP_OK, ['groups' => 'article:read']);
        } catch (NotEncodableValueException $e) {
            return  $this->json([
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/v1/secure/article", name="api_v1_article_delete", methods={"DELETE"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $request = $this->transformJsonContent($request);

        if (!$request->get('id')) {
            return $this->responseBadRequest('Bad parameters');
        }

        $articleId = $request->get('id');

        /** @var Article $article */
        $article = $this->articleRepository->findOneBy(['id' => (int) $articleId]);

        if (!$article)
            return $this->responseContentNotFound();

        $this->entityManager->remove($article);
        $this->entityManager->flush();

        return $this->response(null, Response::HTTP_NO_CONTENT, []);
    }
}