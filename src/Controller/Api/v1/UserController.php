<?php

namespace App\Controller\Api\v1;

use App\Controller\Api\ApiController;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 */
class UserController extends ApiController
{
    /**
     * @Route("/v1/member/users", name="api_v1_users", methods={"GET"})
     *
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function getUsers(UserRepository $userRepository): JsonResponse
    {
        return $this->response($this->$userRepository->findAll(), Response::HTTP_OK, ['groups' => 'article:read']);
    }
}
