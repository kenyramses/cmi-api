<?php

namespace App\Command;

use App\Repository\UserRepository;
use App\Utils\Validator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * A console command that update existing user password.
 */
class UpdateUserCommand extends Command
{
    private SymfonyStyle $io;

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var UserPasswordHasherInterface $passwordHasher */
    private $passwordHasher;

    /** @var UserRepository $usersRepository */
    private $usersRepository;

    /** @var Validator $validator */
    private $validator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordHasherInterface $passwordHasher
     * @param UserRepository $usersRepository
     * @param Validator $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $passwordHasher,
        UserRepository $usersRepository,
        Validator $validator
    ) {
        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
        $this->usersRepository = $usersRepository;
        $this->validator = $validator;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('app:update-user-password')
            ->setDescription(
                'This command Update existing user password. Enter this in console:
                 php bin/console app:register-user firstname lastname email@example.com password'
            )
            ->addArgument('email', InputArgument::OPTIONAL, 'The email of the new user')
            ->addArgument('old_password', InputArgument::OPTIONAL, 'The plain password of the new user')
            ->addArgument('new_password', InputArgument::OPTIONAL, 'The plain password of the new user')
        ;
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('app:update-user-password');

        $email = $input->getArgument('email');
        $oldPassword = $input->getArgument('old_password');
        $newPassword = $input->getArgument('new_password');

        // make sure to validate the user data is correct
        $this->validateUserData($email, $oldPassword, $newPassword);

        // Check if a user with the same email and password exists.
        $existingUser = $this->usersRepository->findOneBy(['email' => $email]);

        if (!$existingUser)
            throw new RuntimeException(sprintf('There is no user registered with the "%s" email.', $email));

        if (!$this->passwordHasher->isPasswordValid($existingUser, $oldPassword))
            throw new RuntimeException(sprintf('The new password provided is not the same with the user registered with "%s" email.', $email));

        // Hash password
        $hashedPassword = $this->passwordHasher->hashPassword($existingUser, $newPassword);
        $existingUser->setPassword($hashedPassword);

        $this->entityManager->persist($existingUser);
        $this->entityManager->flush();

        $this->io->success(sprintf('The password is successfully updated for the user registered with %s email', $email));

        return Command::SUCCESS;
    }

    private function validateUserData($email, $oldPassword, $newPassword): void
    {
        $this->validator->validatePassword($oldPassword);
        $this->validator->validatePassword($newPassword);
        $this->validator->validateEmail($email);
    }
}
