<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Utils\Validator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * A console command that creates users and stores them in the database.
 */
class RegisterUserCommand extends Command
{
    private SymfonyStyle $io;

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var UserPasswordHasherInterface $passwordHasher */
    private $passwordHasher;

    /** @var UserRepository $usersRepository */
    private $usersRepository;

    /** @var Validator $validator */
    private $validator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordHasherInterface $passwordHasher
     * @param UserRepository $usersRepository
     * @param Validator $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $passwordHasher,
        UserRepository $usersRepository,
        Validator $validator
    ) {
        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
        $this->usersRepository = $usersRepository;
        $this->validator = $validator;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('app:register-user')
            ->setDescription('Creates a new "user" as "admin" or anonymous user.')
            ->addArgument('firstname', InputArgument::OPTIONAL, 'The username of the new user')
            ->addArgument('lastname', InputArgument::OPTIONAL, 'The username of the new user')
            ->addArgument('email', InputArgument::OPTIONAL, 'The email of the new user')
            ->addArgument('password', InputArgument::OPTIONAL, 'The plain password of the new user')
            ->addOption('admin', null, InputOption::VALUE_NONE, 'If set, the user is created as an administrator')
        ;
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        if (null !== $input->getArgument('firstname') && null !== $input->getArgument('lastname') && null !== $input->getArgument('email') && null !== $input->getArgument('password') ) {
            return;
        }

        $this->io->title('Add User Command Interactive');
        $this->io->text([
            'If you prefer to not use this interactive wizard, provide the',
            'arguments required by this command as follows:',
            '',
            ' $ php bin/console app:register-user firstname lastname email@example.com password',
            '',
            'Enter the user fields.',
        ]);

        // Ask for the username if it's not defined
        $firstname = $input->getArgument('firstname');
        if (null !== $firstname) {
            $this->io->text(' > <info>Firstname</info>: '.$firstname);
        } else {
            $firstname = $this->io->ask('Firstname', null, [$this->validator, 'validateFirstName']);
            $input->setArgument('firstname', $firstname);
        }

        // Ask for the lastname if it's not defined
        $lastname = $input->getArgument('lastname');
        if (null !== $lastname) {
            $this->io->text(' > <info>Lastname</info>: '.$lastname);
        } else {
            $lastname = $this->io->ask('Lastname', null, [$this->validator, 'validateLastName']);
            $input->setArgument('lastname', $lastname);
        }

        // Ask for the email if it's not defined
        $email = $input->getArgument('email');
        if (null !== $email) {
            $this->io->text(' > <info>Email</info>: '.$email);
        } else {
            $email = $this->io->ask('Email', null, [$this->validator, 'validateEmail']);
            $input->setArgument('email', $email);
        }

        // Ask for the password if it's not defined
        $password = $input->getArgument('password');
        if (null !== $password) {
            $this->io->text(' > <info>Password</info>: '.u('*')->repeat(u($password)->length()));
        } else {
            $password = $this->io->askHidden('Password (your type will be hidden)', [$this->validator, 'validatePassword']);
            $input->setArgument('password', $password);
        }
    }

    /**
     * This method is executed after interact() and initialize(). It usually
     * contains the logic to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('app:register-user');

        $firstname = $input->getArgument('firstname');
        $lastname = $input->getArgument('lastname');
        $email = $input->getArgument('email');
        $plainPassword = $input->getArgument('password');
        $isAdmin = $input->getOption('admin');
        //dd($isAdmin);

        // make sure to validate the user data is correct
        $this->validateUserData($firstname, $lastname, $plainPassword, $email);

        // create the user and hash its password
        $user = new User();
        $user->setFirstName($firstname);
        $user->setLastName($lastname);
        $user->setEmail($email);
        $user->setRoles([$isAdmin ? 'ROLE_ADMIN' : 'ROLE_USER']);

        // Hash password
        $hashedPassword = $this->passwordHasher->hashPassword($user, $plainPassword);
        $user->setPassword($hashedPassword);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->io->success(sprintf('%s was successfully created: %s (%s)', $isAdmin ? 'Administrator user' : 'User', $user->getFirstName(), $user->getEmail()));

        return Command::SUCCESS;
    }

    private function validateUserData($firstname, $lastname, $plainPassword, $email): void
    {
        // Validate password and email if is not this input means interactive.
        $this->validator->validateFirstName($firstname);
        $this->validator->validateLastName($lastname);
        $this->validator->validatePassword($plainPassword);
        $this->validator->validateEmail($email);

        // Check if a user with the same email already exists.
        $existingEmail = $this->usersRepository->findOneBy(['email' => $email]);

        if (null !== $existingEmail) {
            throw new RuntimeException(sprintf('There is already a user registered with the "%s" email.', $email));
        }
    }
}