<?php

namespace App\Utils;

use Symfony\Component\Console\Exception\InvalidArgumentException;

/**
 * Class Validator
 */
class Validator
{
    public function validateFirstName(?string $firstname): string
    {
        if (empty($firstname)) {
            throw new InvalidArgumentException('The firstname can not be empty.');
        }

        return $firstname;
    }

    public function validateLastName(?string $lastname): string
    {
        if (empty($lastname)) {
            throw new InvalidArgumentException('The lastname can not be empty.');
        }

        return $lastname;
    }

    public function validatePassword(?string $plainPassword): string
    {
        if (empty($plainPassword)) {
            throw new InvalidArgumentException('The password can not be empty.');
        }

        return $plainPassword;
    }

    public function validateEmail(?string $email): string
    {
        if (empty($email)) {
            throw new InvalidArgumentException('The email can not be empty.');
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('The email should look like a real email.');
        }

        return $email;
    }
}
