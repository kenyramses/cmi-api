CMI-API TEST
========================
* Fixture
```
php bin/console doctrine:fixtures:load
```

* create new user (interactif)
```
php bin/console app:register-user
```
* creation user oneshot: (--admin option) for create admin
```
php bin/console app:register-user firstname lastname email@example.com password
php bin/console app:register-user firstname lastname email@example.com password --admin
```

* update existing user password
```
php bin/console app:update-user-password email old_password new_password
```

### Connexion Api (générer un token)

* login to api: /api/login_check

```json
{
  "username": "....",
  "password": "...."
}
```

### Some API Routing example

** Article

* put article

```json
{
  "id": 11111111111,
  "content": "...."
}
```
* delete article
```json
{
  "id": 11111111111
}
```

** Comment

* post comment on article (id= id article)

```json
{
  "id": 111111111,
  "message": ".................."
}
```

* delete and approve a comment by his id

```json
{
  "id": 111111111
}
```
